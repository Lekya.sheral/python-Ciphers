import random

def main():
    message = 'This is my secret message.'
    key = random.randint(2, len(message))
    LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
# capitalize the string in message
    message = message.upper()
    print('key used is:', key)
    print('encrypted text is:',caesarCrypt('encrypt', message, LETTERS, key))
    print('decrypted text is:',caesarCrypt('decrypt', caesarCrypt('encrypt', message, LETTERS, key), LETTERS, key))

# run the encryption/decryption code on each symbol in the message string
def caesarCrypt(mode, message, LETTERS, key):
    translated = ''
    for symbol in message:
        if symbol in LETTERS:
        # get the encrypted (or decrypted) number for this symbol
            num = LETTERS.find(symbol) # get the number of the symbol
            if mode == 'encrypt':
                num = num + key
            elif mode == 'decrypt':
                num = num - key

        # handle the wrap-around if num is larger than the length of
        # LETTERS or less than 0
            if num >= len(LETTERS):
                num = num - len(LETTERS)
            elif num < 0:
                num = num + len(LETTERS)

        # add encrypted/decrypted number's symbol at the end of translated
            translated = translated + LETTERS[num]

        else:
        # just add the symbol without encrypting/decrypting
            translated = translated + symbol
    return translated

if __name__ == '__main__':
    main()
