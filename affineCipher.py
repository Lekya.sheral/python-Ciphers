import cryptomath, random
SYMBOLS = """abcdefghijklmnopqrstuvwxyz"""

def main():
    myMessage = """affine cipher"""
    keyA, keyB = getRandomKeys()
    print('keyA generated is:', keyA)
    print('keyB generated is:', keyB)
    print('encrypted message is:', encryptMessage(keyA, keyB, myMessage))
    print('decrypted message is:', decryptMessage(keyA, keyB, encryptMessage(keyA, keyB, myMessage)))

def encryptMessage(keyA, keyB, message):
    #checkKeys(keyA, keyB)
    ciphertext = ''
    for symbol in message:
        if symbol in SYMBOLS:
            symIndex = SYMBOLS.find(symbol)
            ciphertext += SYMBOLS[(symIndex * keyA + keyB) % len(SYMBOLS)]
        else:
            ciphertext += symbol
    return ciphertext

def decryptMessage(keyA, keyB, message):
    #checkKeys(keyA, keyB)
    plaintext = ''
    modInverseOfKeyA = cryptomath.findModInverse(keyA, len(SYMBOLS))
    for symbol in message:
        if symbol in SYMBOLS:
            symIndex = SYMBOLS.find(symbol)
            plaintext += SYMBOLS[(symIndex - keyB) * modInverseOfKeyA % len(SYMBOLS)]
        else:
            plaintext += symbol
    return plaintext


def getRandomKeys():
    while True:
        keyA = random.randint(2, len(SYMBOLS))
        keyB = random.randint(2, len(SYMBOLS))
        if cryptomath.gcd(keyA, len(SYMBOLS)) == 1:
            #return keyA * len(SYMBOLS) + keyB
            return (keyA, keyB)

if __name__ == '__main__':
    main()
'''import sys and also enable these below commented functions if, key is to be taken from user'''

'''def checkKeys(keyA, keyB):
    if keyA == 1:
        sys.exit('The affine cipher becomes incredibly weak when key A is set to 1.')
    if keyB == 0:
        sys.exit('The affine cipher becomes incredibly weak when key B is set to 0.')
    if keyA < 0 or keyB < 0 or keyB > len(SYMBOLS) - 1:
        sys.exit('Key A must be greater than 0 and Key B must be between 0 and %s.' % (len(SYMBOLS) - 1))
    if cryptomath.gcd(keyA, len(SYMBOLS)) != 1:
        sys.exit('Key A (%s) and the symbol set size (%s) are not relatively prime.' % (keyA, len(SYMBOLS)))'''

'''def getKeyParts(key):
    keyA = key // len(SYMBOLS)
    keyB = key % len(SYMBOLS)
    return (keyA, keyB)'''
